#include<stdio.h>
#include<math.h>
#include<point.h>

float distancia(Point p1, Point p2){
	return sqrtf((powf(p2.x-p1.x,2.0) + powf(p2.y-p1.y,2.0) + powf(p2.z-p1.z,2.0)));
};
