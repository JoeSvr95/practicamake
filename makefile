CC=gcc
CFLAGS=-I.
LIBS=-lm
DEPS = point.h
OBJ = main.o point.c

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

points: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean
clean:
	rm -f ./*.o
